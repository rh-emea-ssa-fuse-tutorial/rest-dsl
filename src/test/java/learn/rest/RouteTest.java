package learn.rest;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;

import org.junit.Test;

public class RouteTest extends CamelBlueprintTestSupport {
	
    @Override
    protected String getBlueprintDescriptor() {
        return "OSGI-INF/blueprint/blueprint.xml";
    }


	@Test
	public void testServiceHello() throws Exception{
		String uri = "netty-http:http://localhost:10000/say/hello/junit";
		String response = template.requestBody(uri, null, String.class);
		assertEquals("ups", "Hello junit", response);
	}

	@Test
	public void testRestDslBonjourXmlDataValidationRejectsInput() throws Exception
	{
		//helper variable
		String error = null;

		try
		{	//compose and trigger the request
			String response = template.requestBodyAndHeader(
				"netty-http:http://localhost:10000/say/bonjour", 
				"<tns:bonjour  xmlns:tns=\"http://dsl.learn/\"><random/></tns:bonjour>", 
				"Content-Type", "text/xml", 
				String.class);
		}
		catch(Exception e){error = e.getMessage();};

		//validate failure
		assertTrue("ups", error.contains("Invalid content was found starting with element 'random'"));
	}

	@Test
	public void testRestDslBonjourXmlDataValidationAcceptsInput() throws Exception
	{
		//request XML
		String validXml = 
			"<tns:bonjour xmlns:tns=\"http://dsl.learn/\">"+
			"	<title>Mr.</title>"+
			"	<name>"+
			"		<given>Client</given>"+
			"		<family>Eastwood</family>"+
			"	</name>"+
			"</tns:bonjour>";

		//compose and trigger request
		String response = template.requestBodyAndHeader(
			"netty-http:http://localhost:10000/say/bonjour", 
			validXml, 
			"Content-Type", "text/xml", 
			String.class);

		//validate response
		assertEquals("ups", "Bonjour Mr. Client Eastwood", response);
	}

	@Test
	public void testRestDslBonjourJsonDataValidationRejectsInput() throws Exception
	{
		//helper variable
		String error = null;

		try
		{	//compose and trigger the request
			String response = template.requestBodyAndHeader(
				"netty-http:http://localhost:10000/say/bonjour", 
				"{\"random\":\"value\"}", 
				"Content-Type", "application/json", 
				String.class);
		}
		catch(Exception e){error = e.getMessage();};

		//validate failure
		assertTrue("ups", error.contains("UnrecognizedPropertyException: Unrecognized field \"random\""));
	}

	@Test
	public void testRestDslBonjourJsonDataValidationAcceptsInput() throws Exception
	{
		//prepare multiple headers
		Map<String, Object> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		//headers.put("Accept", "application/json");
		//headers.put("Accept",       "text/xml");

		//compose and trigger request
		String response = template.requestBodyAndHeaders(
			"netty-http:http://localhost:10000/say/bonjour",
			"{\"title\":\"Mr.\",\"name\":{\"given\":\"Client\",\"family\":\"Eastwood\"}}", 
			headers,
			String.class);

		//validate response
		assertEquals("ups", "Bonjour Mr. Client Eastwood", response);
	}
}
