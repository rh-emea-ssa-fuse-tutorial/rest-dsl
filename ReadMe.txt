
TUTORIAL
========

	Use of Camel's REST DSL
	(from scratch, using a text editor, all based on Fuse 6.2.1)

	Index:
	-----
		1. Implementation and Deployment in Fuse
		2. Running standalone (and testing)
		3. Integration with Swagger
		4. POST-XML service
		5. Data validation
		6. POST-JSON service


	And learn interesting bits like:
	--------------------------------
		- display registered services in Fuse
		- browse consumable services from Swagger
		- test services from Swagger
		- define multiple routes reusing the same Netty HTTP port
		- the null safe operator in Camel's Simple expression language
		- validate JSON data against XSD schema.


1. Implementation and Deployment
=================================

The goal in this first section is to focus on what Camel's REST DSL looks like and
how to implement and deploy in Fuse a simple GET service. Further sections in this 
tutorial will be dedicated to more elaborated REST DSL service implementations. 

1.1 Create skeleton

	mvn archetype:generate                   \
	  -DarchetypeGroupId=org.apache.camel.archetypes  \
	  -DarchetypeArtifactId=camel-archetype-blueprint \
	  -DarchetypeVersion=2.15.1.redhat-621084  \
	  -DgroupId=learn.rest                  \
	  -DartifactId=rest-dsl

	(there is a bug in the JUnit, remove the slash in "/OSGI-INF...")

	Note: the archetype will generate some code that may enter in conflict
		  with our tutorial, simply remove the non-relevant code.


1.2 Add the REST DSL definitions inside the CamelContext in Blueprint

	a. Add in the 'camelContext' the REST configuration:
		
			<restConfiguration component="netty-http" host="localhost" port="10000"/>

	   Note the use of 'netty-http' as the transport to be used.


	b. Define the REST DSL service

			<rest path="/say">
				<get uri="hello/{name}">
					<to uri="direct:service-hello"/>
				</get>
			</rest>

	   Note {name} is defined as an input parameter. It gets mapped to a header.
	   The service when deployed will be available using the following address:

			> http://localhost:10000/say/hello/clientName


	c. And finally the Camel route implementing the service above defined
	   
			<route id="service-hello">
			  <from uri="direct:service-hello"/>
			  <setBody>
				<simple>Hello ${header.name}</simple>
			  </setBody>
			</route>

	   Note how the input parameter {name} is used to compose the response.


1.3 Install in Maven and deploy in Fuse (and start)

	a. mvn clean install -DskipTests=true

	b. to deploy in Fuse, we need to ensure first 'netty-http' is enabled.
	   Execute the following commands within Fuse to complete the instalation.

			JBossFuse:karaf@root> features:install camel-netty-http

			  [ 265] [Active     ] [            ] [       ] [   50] Netty (3.9.6.Final)
			  [ 266] [Active     ] [            ] [       ] [   50] camel-netty (2.15.1.redhat-621084)
			  [ 267] [Active     ] [            ] [       ] [   50] camel-netty-http (2.15.1.redhat-621084)

			JBossFuse:karaf@root> install -s mvn:learn.rest/rest-dsl/1.0.0

	   Once deployed you should be able to confirm the service was registered.
	   Execute the following command to list deployed REST services:

			JBossFuse:karaf@root> rest-registry-list camel-1 

			 Base Path      Uri Template   Method         State          Route   
			 ---------      ------------   ------         -----          ------  
			 /say           hello/{name}   get            Started        route1


1.4 From a browser, test the service:

	With the address:

		http://localhost:10000/say/hello/client

	It should return:

		"Hello client"



2. Testing JUnits and running standalone
========================================

2.1 Update the POM file:

	JBoss Fuse (the engine) already contains all the dependencies required by the project.
	But the project won't run without a number of dependencies if:
		a. running JUnits (maven test phase)
		b. running standalone (with 'mvn camel:run')

	For the previously defined DSL configuration, the 'netty-http' needs to be loaded:

		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-netty-http</artifactId>
		  <version>${camel.version}</version>
		</dependency>


2.2 To run standalone:

	(ensure Fuse is down)

	a. run the command:
		> mvn clean camel:run

	b. from a web browser you should be able to hit the service via the URL:
		> http://localhost:10000/say/hello/{name}
	   e.g.
		> http://localhost:10000/say/hello/world


2.3 To run Junits

	a. add the following JUnit using 'netty-http' to hit the service:

			@Test
			public void testServiceHello() throws Exception{
				String uri = "netty-http:http://localhost:10000/say/hello/junit";
				String response = template.requestBody(uri, null, String.class);
				assertEquals("ups", "Hello junit", response);
			}

		Note: the body sent is 'null', this ensures Netty uses the HTTP GET method on the request.


	b. to run the test execute the maven command:
		> mvn clean test



3. Integration with Swagger
===========================

	Integration with Swagger allows users to explore available REST services.

		(Ref. documentation:
		 > https://access.redhat.com/documentation/en-US/Red_Hat_JBoss_Fuse/6.2.1/html/Apache_Camel_Development_Guide/RestServices-Swagger.html)


3.1 Enable Swagger in Fuse

	Fuse includes Swagger modules, but need to be explecitely enabled.

		(ref. knowledge-base article:
		 > https://access.redhat.com/solutions/2161611)

	Run from Fuse the following commands:

	 >features:install fabric-hawtio-swagger

		[ 268] [Active     ] [            ] [       ] [   80] Fabric8 :: Git :: Hawtio (1.2.0.redhat-621084)
		[ 269] [Active     ] [            ] [       ] [   80] hawtio :: project (1.4.0.redhat-621084)

	 >features:install camel-swagger

		[ 270] [Active     ] [            ] [       ] [   50] Apache ServiceMix :: Bundles :: paranamer (2.6.0.1)
		[ 271] [Active     ] [            ] [       ] [   50] camel-swagger (2.15.1.redhat-621084)


3.2 Define the Swagger servlet

	To expose the service in Swagger, define the following Servlet in the Blueprint file:

		<service interface="javax.servlet.http.HttpServlet">
		    <service-properties>
		        <entry key="alias" value="/rest/api-docs/*"/>
		        <entry key="init-prefix" value="init."/>
		        <entry key="init.base.path" value="http://localhost:10000"/>
		        <entry key="init.api.title" value="Camel Rest Tutorial API"/>
		        <entry key="init.api.version" value="1.0"/>
		        <entry key="init.api.description"
		            value="Camel Rest Tutorial with Swagger that provides an User REST service"/>
		    </service-properties>
		    <bean class="org.apache.camel.component.swagger.DefaultCamelSwaggerServlet" />
		</service>

	This servlet obtains the REST service information from the Camel context.
	Swagger can then interact with the Servlet to get the information. 


3.3 Enable CORS in the REST configuration

	Swagger would require the enablement of CORS headers to properly work (see references).
	Otherwise it may not properly explore the REST operations.

		(refs:
		  - http://camel.apache.org/swagger.html
		  - https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)

	Add the attribute [enableCORS="true"] to the 'restConfiguration' definition.
	It should look as follows:

		<restConfiguration component="netty-http" host="localhost" port="10000" enableCORS="true"/>


3.4 Install and redeploy

		a. > mvn clean install -DskipTests=true
		b. > update [bundle-id]


3.5 Explore available services from Swagger's interface

	From a browser, display the Swagger interface connecting to:

		http://localhost:8181/hawtio-swagger

	Then from Swagger's address bar, use the following URI to display the REST service

		http://localhost:8181/rest/api-docs

	On screen, click 'say' and you should see listed the 'hello' service deployed in Fuse:

		> Camel Rest Tutorial API
		> Camel Rest Tutorial with Swagger that provides an User REST service
		>
		>	say:
		>		[GET] /say/hello/{name}

	Clicking on [/say/hello/{name}] will reveal the interface details of the service.
	Fill in the parameter 'name' value and click 'Try it out!'.

	For example, entering the value 'client' should trigger a request against the service in Fuse 
	and return:

		> Request URL
		    http://localhost:10000/say/hello/client

		> Response Body
			Hello client

		> Response Code
			200

		> Response Headers
			{}


3.6 Running standalone and testing

	When running standalone or testing, the new Swagger servlet added to the Blueprint will require the following
	Maven dependencies:

		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-jetty</artifactId>
		  <version>${camel.version}</version>
		</dependency>
		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-swagger</artifactId>
		  <version>${camel.version}</version>
		</dependency>

	

4. POST-XML service
===================

	This section demonstrates how to define a POST operation to handle complex-data (XML) using
	Camel's REST DSL.

4.1 POM file additions

	REST DSL uses JAXB to handle data structures. The POM file needs therefore to include the following
	dependency:

		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-jaxb</artifactId>
		  <version>${camel.version}</version>
		</dependency>

	The example follows an 'XSD-first' approach to define the data structure and uses the following
	plugin to generate JAXB sources from XSD definitions:

		<plugin>
			<groupId>org.codehaus.mojo</groupId>
			<artifactId>jaxb2-maven-plugin</artifactId>
			<executions>
				<execution>
				    <id>xjc-schema1</id>
				    <goals>
				        <goal>xjc</goal>
				    </goals>
					<configuration>
						<sources>
							<source>src/main/resources/xsd</source>
						</sources>
					</configuration>
				</execution>
			</executions>
		</plugin>

	Note the XSD definitions will be picked up from the <sources> configuration, in the
	above definition, XSDs will be read from the folder 'xsd' under 'resources'.


4.2 XSD definition

	The example uses the following data structure definition:

		<xs:element name="bonjour">
		  <xs:complexType>
			<xs:sequence>
			  <xs:element minOccurs="0" name="title" type="xs:string"/>
			  <xs:element minOccurs="0" name="name" type="tns:name"/>
			</xs:sequence>
		  </xs:complexType>
		</xs:element>

		<xs:complexType name="name">
		  <xs:sequence>
		    <xs:element minOccurs="0" name="given" type="xs:string"/>
		    <xs:element minOccurs="0" name="family" type="xs:string"/>
		  </xs:sequence>
		</xs:complexType>

	with targetNamespace="http://dsl.learn/".

		Important:
		----------
		> Things you should know when converting an XSD to JAVA:
			ref: http://blog.bdoughan.com/2012/07/jaxb-and-root-elements.html
		> Not understanding JAXB rules may get you in trouble with errors like:
			"missing an @XmlRootElement annotation"

	The above XSD will result during build process in the following Java objects created:
		> learn.dsl.Bonjour
		> learn.dsl.Name


4.3 Blueprint definitions 

	a. Define the REST DSL service

			<rest path="/say">
				...
				<post uri="bonjour" type="learn.dsl.Bonjour" bindingMode="xml" consumes="text/xml">
					<to uri="direct:service-bonjour"/>
				</post>
			</rest>

	   Note
		> the new 'post' definition is added alongside the previously defined 'get' opeartion.
		> the parameter 'type' indicates the data type expected as an input.
		> the binding mode indicates the type of data to handle (XML).
.
	   The service when deployed will be available using the following address:

			> http://localhost:10000/say/bonjour


	b. Define Camel route implementing the service above defined
	   
			<route id="service-bonjour">
			  <from uri="direct:service-bonjour"/>
			  <setBody>
				<simple>Bonjour ${body?.title} ${body?.name.given} ${body?.name.family}</simple>
			  </setBody>
			</route>

		Note how the response is composed by accessing the JAXB object using Camel's 'Simple' expression language.
		The question mark '?' is a null safe operator to avoid NullPointerException if the body is empty.

		The result of this Camel route returns a string instead of a JAXB object. In summary the new REST service
		created is as follows:

			POST: in  -> XML
			      out <- String


	c. Update REST configuration

		REST DSL expects by default a JAXB object in the body to be converted to XML. To switch OFF the default
		behaviour the parameter 'mustBeJAXBElement' needs to be added to <restConfiguration> as follows:

			<restConfiguration component="netty-http" host="localhost" port="10000" enableCORS="true">
				<dataFormatProperty key="xml.out.mustBeJAXBElement" value="false"/>
			</restConfiguration>

	
	d. CORS workaround

		Swagger helps documenting services. It allows a user to browse through the available consumable
		services, and also allows to test them from its interface.

		Testing from Swagger requires enabling CORS, as indicated early in this tutorial.
		Unfortunately testing REST-DSL POST operations from Swagger seems to be problematic in Fuse 6.2.1
		When Swagger fires a test request, it first sends an OPTIONS request as per specification.

			(ref: https://en.wikipedia.org/wiki/Cross-origin_resource_sharing#How_CORS_works)

		Fuse 6.2.1 is not handling properly the OPTIONS request and not correctly responding to Swagger
		which therefore can't complete the POST service test.

		Note:
			> This problem has been already fixed in further versions of Camel (tested with Camel 2.17).
			> Future versions of Fuse will include the fixes.


		The way to overcome this problem is to add a dedicated Camel route to handle the OPTIONS request
		as follows:
		(ref: https://issues.apache.org/jira/browse/CAMEL-8373)

			<route id="bonjour-options">
			  <from uri="netty-http:http://localhost:10000/say/bonjour?httpMethodRestrict=OPTIONS"/>
			  <to uri="direct:setcors"/>
			</route>

			<route id="setcors">
				<from uri="direct:setcors"/>
				<setHeader headerName="Access-Control-Allow-Origin">
				    <constant>*</constant>
				</setHeader>
				<setHeader headerName="Access-Control-Allow-Methods">
				    <constant>GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, CONNECT, PATCH</constant>
				</setHeader>
				<setHeader headerName="Access-Control-Allow-Headers">
				    <constant>Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers</constant>
				</setHeader>
			</route>

		Note:
		 > the Netty component is used both in the new Camel route and the 'restConfiguration'.
		 > the Netty listener is configured for OPTIONS calls only.
		

4.4 Install and redeploy

		a. > mvn clean install -DskipTests=true
		b. > update [bundle-id]

	   Once deployed you should be able to confirm the new service was registered.
	   Execute the following command to list deployed REST services:

			JBossFuse:karaf@root> rest-registry-list camel-1 

			 Base Path      Uri Template   Method         State          Route   
			 ---------      ------------   ------         -----          ------ 
			 /say           bonjour        post           Started        route2 
			 /say           hello/{name}   get            Started        route1


4.5 Explore available services from Swagger's interface

	From a browser, display the Swagger interface connecting to:

		http://localhost:8181/hawtio-swagger

	Then from Swagger's address bar, use the following URI to display the REST service

		http://localhost:8181/rest/api-docs

	On screen, click 'say' and you should see listed the 'bonjour' service deployed in Fuse:

		> Camel Rest Tutorial API
		> Camel Rest Tutorial with Swagger that provides an User REST service
		>
		>	say:
		>		[POST] /say/bonjour
		>		[GET]  /say/hello/{name}

	Clicking on [/say/bonjour] will reveal the interface details of the service.
	The JSON model schema shown matches the Bonjour's data structure from the XSD definition.

		Note:
		You'll see the POST 'bonjour' service describes an input JSON data type instead of XML, this is
		a limitation resolved in Swagger 2.0, but Fuse 6.2.1 is bundled with Swagger 1.x

			(refs:
			  - http://swagger.io/specification/#xmlObject
			  - https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#xmlObject)

	You'll also notice the content-type gets predefined as per the service definition in
	Blueprint where the parameter consumes="text/xml" is set.

	Fill in the parameter 'body' with the sample below and click 'Try it out!':

		<ns2:bonjour xmlns:ns2="http://dsl.learn/">
			<title>Mr.</title>
			<name>
				<given>Client</given>
				<family>Eastwood</family>
			</name>
		</ns2:bonjour>


	Entering the above XML request and triggering the test against the service in Fuse 
	should result in:

		> Request URL
		    http://localhost:10000/say/bonjour

		> Response Body
			Bonjour Mr. Client Eastwood

		> Response Code
			200

		> Response Headers
			{
			  "Content-Type": "text/xml; charset=UTF-8"
			}


5. REST DSL Data Validation
============================

	In the previous chapter a new XML service was defined and tested.
	But there was a catch, the REST-server is not fully validating the input data.

	- The input "Not XML"
	  would throw the following error:
		> [javax.xml.stream.XMLStreamException: ParseError at [row,col]:[1,1]
		  Message: Content is not allowed in prolog.]


	- The input "<random/>"
	  would throw the following error:
		> UnmarshalException: unexpected element (uri:"", local:"random").
		  Expected elements are <{http://dsl.learn/}bonjour>

	All the above results are expected behaviours, however

	- The input:

		<tns:bonjour xmlns:tns="http://dsl.learn/">
			<random/>
		</tns:bonjour>

	should also be rejected, however the Camel's REST-DSL accepts it and hands over to the dedicated route,
	leading to unpredictable behaviour.

		> Note we used the null safe operator '?' in the simple expression which avoids a NullPointerException. 

	The following section shows how to enforce data validation. 


5.1 Update the <restConfiguration> as follows:

	Include the following property inside the <restConfiguration>

		<dataFormatProperty key="xml.in.schema" value="xsd/Bonjour.xsd"/>

	The above property tells Camel's REST DSL to validate the input XML against the indicated XSD schema.


5.2 To run standalone:

	(Ensure Fuse is down)

	a. run the command:
		> mvn clean camel:run

	b. from Swagger you should be able to test the service:
		> [POST] /say/bonjour

	c. posting the following XML:

			<tns:bonjour xmlns:tns="http://dsl.learn/">
				<random/>
			</tns:bonjour>

		should be rejected with:

			"Invalid content was found starting with element 'random'."
	
	d. posting the following XML:

			<ns2:bonjour xmlns:ns2="http://dsl.learn/">
				<title>Mr.</title>
				<name>
					<given>Client</given>
					<family>Eastwood</family>
				</name>
			</ns2:bonjour>

		should return:

			"Bonjour Mr. Client Eastwood"


5.3 Include JUnits to validate implementation

	a. The following JUnit ensures invalid XML is rejected

			@Test
			public void testRestDslBonjourXmlDataValidationRejectsInput() throws Exception
			{
				String error = null;
				try
				{	//compose and trigger the request
					String response = template.requestBodyAndHeader(
						"netty-http:http://localhost:10000/say/bonjour", 
						"<tns:bonjour  xmlns:tns=\"http://dsl.learn/\"><random/></tns:bonjour>", 
						"Content-Type", "text/xml", 
						String.class);
				}
				catch(Exception e){error = e.getMessage();};
				assertTrue("ups", error.contains("Invalid content was found starting with element 'random'"));
			}


	b. The following JUnit ensures valid XML is accepted

			@Test
			public void testRestDslBonjourXmlDataValidationAcceptsInput() throws Exception
			{
				String validXml = 
					"<tns:bonjour xmlns:tns=\"http://dsl.learn/\">"+
					"	<title>Mr.</title>"+
					"	<name>"+
					"		<given>Client</given>"+
					"		<family>Eastwood</family>"+
					"	</name>"+
					"</tns:bonjour>";
				String response = template.requestBodyAndHeader(
					"netty-http:http://localhost:10000/say/bonjour", 
					validXml, 
					"Content-Type", "text/xml", 
					String.class);
				assertEquals("ups", "Bonjour Mr. Client Eastwood", response);
			}


6. POST-JSON service
====================

	This section demonstrates how to define a POST-JSON operation using	Camel's REST DSL.
	The example below extends the POST-XML service implemented earlier in the tutorial.
	The service will be consumable by choosing XML or JSON as content type. 

6.1 POM file additions

	When testing or running standalone, REST DSL will require 'json-jackson' to handle JSON data.
	The POM file needs therefore to include the following dependency:

		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-jackson</artifactId>
		  <version>${camel.version}</version>
		</dependency>


6.2 Blueprint definitions 

	a. Update as follows the POST service to enable it to handle JSON data:

		<rest path="/say">
			...
			<post uri="bonjour" type="learn.dsl.Bonjour" bindingMode="json_xml" consumes="text/xml, application/json">
				<to uri="direct:service-bonjour"/>
			</post>
		</rest>

	   Note
		> the parameter 'bindingMode' is now set to both JSON and XML.
		> the parameter 'consumes' now also includes the type 'application/json'.
.
	   The service when deployed will be available using the same address:

			> http://localhost:10000/say/bonjour

	b. Update the Camel route 'service-bonjour' to set the header 'Content-Type: text/xml' explicitely:

			<route id="service-bonjour">
			  <from uri="direct:service-bonjour"/>
			  <setHeader headerName="Content-Type">
				<constant>text/xml</constant>
			  </setHeader>
			  <setBody>
				<simple>Bonjour ${body?.title} ${body?.name.given} ${body?.name.family}</simple>
			  </setBody>
			</route>

		As REST-DSL receives JSON, when responding it will by default convert the String body to JSON
		by wrapping it with double quotes. An explicit setting of the content-type overrides the default behaviour.

		Note: a client may still include the header 'Accept: application/json' indicating it wants a JSON response.
		      REST-DSL in that case will anyway wrap the String with quotes.
              There is a JIRA ticket to address this scenario:
		        - https://issues.apache.org/jira/browse/CAMEL-9625


	c.	In summary, the updated REST POST service can now be described as:

			POST: in  -> XML/JSON
			      out <- String
		

		Note the input JSON data will still be validated against the XSD schema.
		The data validation configuration configured earlier for XML inputs also applies to JSON inputs.


6.3 Install and redeploy

		a. > mvn clean install -DskipTests=true

		b. Ensure the feature 'camel-jackson' is installed in Fuse

			JBossFuse:karaf@root> features:install camel-jackson
			JBossFuse:karaf@root> list | grep jackson
			  [ 281] [Active     ] [            ] [       ] [   50] camel-jackson (2.15.1.redhat-621084)

		c. > update [bundle-id]

	   Once deployed you should be able to confirm the updated service was registered.
	   Execute the following command to list deployed REST services:

			JBossFuse:karaf@root> rest-registry-list camel-1 

			 Base Path      Uri Template   Method         State          Route   
			 ---------      ------------   ------         -----          ------ 
			 /say           bonjour        post           Started        route2 
			 /say           hello/{name}   get            Started        route1


6.4 Explore available services from Swagger's interface

	From a browser, display the Swagger interface connecting to:

		http://localhost:8181/hawtio-swagger

	Then from Swagger's address bar, use the following URI to display the REST service

		http://localhost:8181/rest/api-docs

	On screen, click 'say' and you should see listed the 'bonjour' service deployed in Fuse:

		> Camel Rest Tutorial API
		> Camel Rest Tutorial with Swagger that provides an User REST service
		>
		>	say:
		>		[POST] /say/bonjour
		>		[GET]  /say/hello/{name}

	Clicking on [/say/bonjour] will reveal the interface details of the service.
	The JSON model schema shown matches the Bonjour's data structure from the XSD definition.

	Notice you can now choose the content-type to be either XML or JSON as per the service definition in
	Blueprint where the parameter 'consumes' is set.

	Choose 'application/json', fill in the parameter 'body' with the sample below and click 'Try it out!':

		{
		  "title": "Mr.",
		  "name": {
			"given": "Client",
			"family": "Eastwood"
		  }
		}

	Entering the above JSON request and triggering the test against the service in Fuse 
	should result in:

		> Request URL
		    http://localhost:10000/say/bonjour

		> Response Body
			"Bonjour Mr. Client Eastwood"

		> Response Code
			200

		> Response Headers
			{
			  "Content-Type": "application/json; charset=UTF-8"
			}

	Please note the response body is wrapped with double quotes. This is because Swagger is including
	the request header 'Accept: application/json' which causes Camel's REST-DSL to convert the response String to JSON.


6.5 Include JUnits to validate implementation

  It is a 'best practice' to always include JUnits, they might help you to uncover hidden nasty problems !!

	a. The following JUnit ensures invalid JSON is rejected

			@Test
			public void testRestDslBonjourJsonDataValidationRejectsInput() throws Exception
			{
				String error = null;
				try
				{	//compose and trigger the request
					String response = template.requestBodyAndHeader(
						"netty-http:http://localhost:10000/say/bonjour", 
						"{\"random\":\"value\"}", 
						"Content-Type", "application/json", 
						String.class);
				}
				catch(Exception e){error = e.getMessage();};
				assertTrue("ups", error.contains("UnrecognizedPropertyException: Unrecognized field \"random\""));
			}

	b. The following JUnit ensures valid JSON is accepted

			@Test
			public void testRestDslBonjourJsonDataValidationAcceptsInput() throws Exception
			{
				Map<String, Object> headers = new HashMap<>();
				headers.put("Content-Type", "application/json");
				String response = template.requestBodyAndHeaders(
					"netty-http:http://localhost:10000/say/bonjour",
					"{\"title\":\"Mr.\",\"name\":{\"given\":\"Client\",\"family\":\"Eastwood\"}}", 
					headers,
					String.class);
				assertEquals("ups", "Bonjour Mr. Client Eastwood", response);
			}






